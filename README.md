# RUI

Someone told me it's okay to build UI/UX in your favourite language. I took that
to mean reinventing the wheel so that's what we'll do.

## Concept

Component based style definition. Component based structure definition. Still
separate like CSS and HTML. Intelligently coupled at point of use.

## Example

Please find the example in this repo. The implementation hasn't started yet
but this example embodies the idea.

## Author

Luke Morton

## License

This idea is MIT.
